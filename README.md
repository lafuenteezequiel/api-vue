# api-vue

Implementação de estrutura criada pelo Ezequiel Lafuente, utilizando:

Laravel 5.8 e Vue 2 e MySQL

php 7.2
node 8.10
npm 3.5.2

Webpack
Vuex
Vue-Resource
Vue-Router
Vuetify
VeeValidate
Lodash



Fontes:

Link itinerante para aprender mais sobre vuex:
https://scrimba.com/playlist/pnyzgAP

Doc sobre axios
https://github.com/axios/axios

Vue com Axios
https://br.vuejs.org/v2/cookbook/using-axios-to-consume-apis.html#Exemplo-Base
https://www.schoolofnet.com/forum/topico/vue-resource-ou-axios-9153

Pacote para validações
https://vuelidate.netlify.com/#sub-form-submission

Melhores pacotes para trabalhar com vue
https://www.netguru.com/blog/top-vue.js-open-source-projects-the-best-libraries-tools-and-packages

Ordenação:
https://lodash.com/

Grupos brasileiros no telegran
https://t.me/vuetifybr
https://t.me/vuejsbrasil

Procurar pacotes
https://curated.vuejs.org/

Acl
https://www.schoolofnet.com/forum/topico/acl-com-vue-js-4893

