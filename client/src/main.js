import { sync } from 'vuex-router-sync'
import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import App from '@/App'
import routes from '@/router/index'
import VuexStore from '@/vuex/store'
import VeeValidate from 'vee-validate'
import LoginInterceptors from '@/components/Login/Interceptor'
import Middlewre from '@/router/middlewares'
import 'vuetify/dist/vuetify.min.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@/template/utils/filters'

Vue.use(VeeValidate)
Vue.use(Vuex)
Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(Vuetify)

Vue.http.options.root = process.env.SERVER

const store = new Vuex.Store(VuexStore)

const router = new VueRouter({
  mode: 'history', // Assim ele perde o padr�o hash, onde ele concatena sempre um "#", aquilo serve p/ o servidor n�o recarregar a cada mudan�a de rota
  routes
})

// chamada dos middlewares
Middlewre.validateAuth(router)
Middlewre.redirectProducts(router)

LoginInterceptors.check_empty_token(router)
LoginInterceptors.check_auth(router)

sync(store, router)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  watch: {
    '$route' (to, from) {
      LoginInterceptors.check_empty_token(router)
      this.$store.dispatch('clearRegistries')
    }
  },
  template: '<App/>',
  components: { App }
})
