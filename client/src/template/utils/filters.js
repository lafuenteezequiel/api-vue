import Vue from 'vue'

const formatarData = function (date, format = 'pt-br') {
  if (date !== undefined) {
    if (format === 'pt-br') {
      return (date.substr(0, 10).split('-').reverse().join('/'))
    } else {
      return (date.substr(0, 10).split('/').reverse().join('-'))
    }
  }
}

const numberToReal = function (valor) {
  var numero = valor.toFixed(2).split('.')
  numero[0] = 'R$ ' + numero[0].split(/(?=(?:...)*$)/).join('.')
  return numero.join(',')
}

Vue.filter('formatar-data', formatarData)
Vue.filter('formatar-moeda', numberToReal)
