import Pagination from '@/vuex/modules/pagination'
import Category from '@/vuex/modules/categories'
import Product from '@/vuex/modules/products'

export default {
  modules: {
    pagination: Pagination,
    category: Category,
    product: Product
  }
}
