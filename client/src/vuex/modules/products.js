import Vue from 'vue'

const state = {
  productList: [],
  productView: {}
}

const mutations = {
  updateProductsList (state, data) {
    state.productList = data
  },
  updateProductsView (state, data) {
    state.productView = data
  }
}

const actions = {
  getProducts (context) {
    Vue.http.get('api/products').then(response => {
      context.commit('updateProductsList', response.data)
    })
  },
  getProduct (context, id) {
    Vue.http.get('api/products/' + id).then(response => {
      context.commit('updateProductsView', response.data)
    })
  },
  newProduct (context, data) {
    Vue.http.post('api/products', data)
  },
  updateProduct (context, params) {
    Vue.http.put('api/products/' + params.id, params)
  },
  removeProduct (context, id) {
    Vue.http.delete('api/products/' + id)
  }
}

export default {
  state,
  mutations,
  actions
}
