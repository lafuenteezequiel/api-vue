import Vue from 'vue'

const state = {
  categoryList: [],
  categoryView: {}
}

const mutations = {
  updateCategoryList (state, data) {
    state.categoryList = data
  },
  updateCategoryView (state, data) {
    state.categoryView = data
  }
}

const actions = {
  getCategories (context) {
    Vue.http.get('api/categories').then(response => {
      context.commit('updateCategoryList', response.data)
    })
  },
  getCategory (context, id) {
    Vue.http.get('api/categories/' + id).then(response => {
      context.commit('updateCategoryView', response.data)
    })
  },
  newCategory (context, data) {
    Vue.http.post('api/categories', data)
  },
  updateCategory (context, params) {
    Vue.http.put('api/categories/' + params.id, params)
  },
  removeCategory (context, id) {
    Vue.http.delete('api/categories/' + id)
  }
}

export default {
  state,
  mutations,
  actions
}
