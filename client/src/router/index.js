import Login from '@/components/Login/Form'
import ListCategory from '@/components/Categories/List'
import ViewCategory from '@/components/Categories/View'
import EditCategories from '@/components/Categories/Edit'
import CreateCategories from '@/components/Categories/Create'
import ListProduct from '@/components/Products/List'
import ViewProduct from '@/components/Products/View'
import EditProduct from '@/components/Products/Edit'
import CreateProduct from '@/components/Products/Create'

const routes = [
  {path: '/login', component: Login},
  {path: '/categorias', component: ListCategory, meta: {requiresAuth: true}},
  {path: '/categorias/novo', component: CreateCategories, meta: {requiresAuth: true}},
  {path: '/categorias/:id/editar', component: EditCategories, meta: {requiresAuth: true}},
  {path: '/categorias/:id', component: ViewCategory, meta: {requiresAuth: true}},
  {path: '/produtos', component: ListProduct, meta: {requiresAuth: true}},
  {path: '/produtos/novo', component: CreateProduct, meta: {requiresAuth: true}},
  {path: '/produtos/:id/editar', component: EditProduct, meta: {requiresAuth: true}},
  {path: '/produtos/:id', component: ViewProduct, meta: {requiresAuth: true}}
]

export default routes
