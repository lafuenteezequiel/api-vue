export default {
  validateAuth: function (router) {
    router.beforeEach((to, from, next) => {
      let requiresAuth = to.meta.requiresAuth || false

      if (requiresAuth) {
        if (!window.authenticate) {
          return next({path: '/login'})
        }
      }
      return next()
    })
  },
  redirectProducts: function (router) {
    router.beforeEach((to, from, next) => {
      if (to.fullPath === '/') {
        return next({path: '/produtos'})
      }
      return next()
    })
  }
}
