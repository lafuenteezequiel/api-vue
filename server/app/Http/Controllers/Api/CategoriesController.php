<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CategoryRepositoryInterface;

class CategoriesController extends Controller
{
    use \App\Http\Controllers\ApiControllerTrait;

    protected $model;
    protected $repository;

    public function __construct(CategoryRepositoryInterface $category, Category $model)
    {
        $this->model = $model;
        $this->category = $category;
    }
}