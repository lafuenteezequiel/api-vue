<?php

namespace App\Http\Controllers\Api;

use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryInterface;
use App\Repositories\ProductRepository;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    use \App\Http\Controllers\ApiControllerTrait;

    protected $model;
    protected $repository;
    protected $relationships = ['category'];
    protected $fildsignore = ['data_cadastro'];

    public function __construct(ProductRepositoryInterface $product , Product $model)
    {
        $this->model = $model;
        $this->product = $product;
    }

    public function teste()
    {
        return $this->product->findAll();
    }
}