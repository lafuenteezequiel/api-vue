<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Product extends Model
{
    protected $table = 'tb_produto';
    protected $fillable = ['id_categoria_produto', 'data_cadastro','nome_produto','valor_produto'];

    public function category()
    {
        return $this->hasOne('App\Models\Category','id','id_categoria_produto');
    }

    public function setDataCadastroAttribute($value)
    {
        $this->attributes['data_cadastro'] = Carbon::createFromFormat('d/m/Y', $value)->toDateString();
    }
}
